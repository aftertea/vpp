package org.prototype.primecount.foundation;

public interface IProgressWatch
{
    void start(final String message);

    void defineSteps(final int... stepWeights);

    void endStep();

    void jobProgressed(final Job job);

    void jobJustStartd();
    
    void done();
}
