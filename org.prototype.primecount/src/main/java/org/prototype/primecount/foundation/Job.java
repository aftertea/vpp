package org.prototype.primecount.foundation;

public final class Job //NOPMD
{
    private static final int ZERO = 0;
    private final Step[] steps;
    private boolean inProgress;
    private String message;
    private Step currentStep;
    private int currentStepIndex;
    private int progressed;

    Job()
    {
        super();

        this.steps = new Step[] {new Step(99) };
        init();
    }

    Job(final Step... stepArry)
    {
        assert null != stepArry : "Parameter 'stepArry' of 'Job''s ctor must not be null";

        this.steps = stepArry.clone();
        init();
    }

    private void init()
    {
        this.currentStepIndex = ZERO;
        this.currentStep = this.steps[this.currentStepIndex];
    }

    public void start(final String msg)
    {
        assert !isInProgress() : "Already in progress";
        assert null != this.steps : "'steps' of method 'start' must not be null";
        this.inProgress = true;
        this.message = msg;
        init();
    }

    private boolean isInProgress()
    {
        return this.inProgress;
    }

    public void endStep()
    {
        assert this.steps.length - 1 <= this.currentStepIndex : "Job has already progressed all the steps";

        final int depth = this.currentStep.end();
        this.progressed += depth;
        //TODO logging required
        //assert this.steps.length > this.currentStepIndex : "No steps left any more";
        if (this.steps.length < this.currentStepIndex )
        {
            return;
        }
        this.currentStep = this.steps[++this.currentStepIndex];
    }

    public int howMuchProgressed()
    {
        return this.progressed;
    }

    @Override
    public String toString()
    {
        return this.message;
    }
}
