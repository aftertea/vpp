package org.prototype.primecount.foundation;

public final class Step //NOPMD
{
    private int depth;

    public Step(final int weight)
    {
        assert 0 < weight : "Parameter 'weight' of 'Step''s ctor must ne greater than 0";

        this.depth = weight;
    }

    public int end()
    {
        assert 0 < this.depth : "Field 'depth' in method 'end' must ne greater than 0";

        return this.depth - 1;
    }

    public void defineDepth(final int weight)
    {
        assert 0 < weight : "Parameter 'weight' of method 'setDepth' must be greater than 0";

        this.depth = weight;
    }
}
