package org.prototype.primecount.foundation;

import java.util.HashMap;
import java.util.Map;

public final class SimpleEventManager
{
    private static SimpleEventManager instance;

    private final Map<Class<?>, IEventHandler<?>> event2Handler;

    private SimpleEventManager()
    {
        super();

        this.event2Handler = new HashMap<>(10);
    }

    public static SimpleEventManager getInstance()
    {
        synchronized (SimpleEventManager.class)
        {
            if (null == instance)
            {
                instance = new SimpleEventManager();
            }
            return instance;
        }
    }

    public void attach(final IEventHandler<?> handler)
    {
        this.event2Handler.put(((AbstractEventHandler<?>) handler).getEventClass(), handler); //NOPMD
    }

    @SuppressWarnings({"rawtypes", "unchecked" })
    public <T extends AbstractEvent> void dispatch(final T event)
    {
        assert null != event : "Parameter 'event' of method 'dispatch' must not be null";

        final IEventHandler handler = this.event2Handler.get(event.getClass());
        final T castedEvent = event;
        handler.handle(castedEvent); //NOPMD
    }
}
