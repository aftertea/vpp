package org.prototype.primecount.foundation;

public interface IEventHandler<T>
{
    void handle(T event);
}
