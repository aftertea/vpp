package org.prototype.primecount.foundation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.converter.Converter.ConversionException;

@SuppressWarnings("serial")
public final class ProtoInvalidValueException extends InvalidValueException
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ProtoInvalidValueException.class);

    private final ConversionException cnvrsExcep;

    public ProtoInvalidValueException(final String message, final ConversionException exception)
    {
        super(message);

        assert null != exception : "Parameter 'exception' of 'ProtoInvalidValueException''s ctor must not be null";

        this.cnvrsExcep = exception;
        LOGGER.error("Original conversion exception: '{}'", this.cnvrsExcep);
    }

    public ConversionException getOriginalConversionException()
    {
        return this.cnvrsExcep;
    }
}
