package org.prototype.primecount.foundation;

public abstract class AbstractEventHandler<T extends AbstractEvent> implements IEventHandler<T>
{
    private final Class<T> eventClass;

    protected AbstractEventHandler(final Class<T> eventClassPassed)
    {
        assert null != eventClassPassed : "Parameter 'eventClassPassed' of 'AbstractEventHandler''s ctor must not be null";

        this.eventClass = eventClassPassed;
    }

    public final Class<T> getEventClass()
    {
        return this.eventClass;
    }

    @Override
    public abstract void handle(final T event);
}
