package org.prototype.primecount.foundation;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractProgressWatch implements IProgressWatch
{
    private Job job;

    protected AbstractProgressWatch()
    {
        super();

        this.job = new Job();
    }

    @Override
    public void start(final String message)
    {
        assert null != this.job : "'job' of method 'start' must not be null";
        this.job.start(message);
        jobJustStartd();
    }

    @Override
    public void defineSteps(final int... stepWeights) //NOPMD
    {
        assert null != stepWeights : "Parameter 'stepWeights' of method 'defineSteps' must not be null";

        final List<Step> stepLst = new ArrayList<>(stepWeights.length);
        for (final int nextW : stepWeights) //NOPMD
        {
            stepLst.add(makeStep(nextW));
        }
        this.job = new Job(stepLst.toArray(new Step[stepLst.size()]));
    }

    private static Step makeStep(final int nextW)
    {
        return new Step(nextW);
    }

    @Override
    public void endStep()
    {
        this.job.endStep();
        jobProgressed(this.job); //NOPMD
    }
}
