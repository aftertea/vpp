package org.prototype.primecount.foundation;

import java.math.BigInteger;

public final class BigIntegerCounter
{
    private BigInteger count;

    public BigIntegerCounter()
    {
        this.count = BigInteger.ZERO;
    }

    public void increment()
    {
        synchronized (this)
        {
            this.count = this.count.add(BigInteger.valueOf(1));
        }
    }

    @Override
    public String toString()
    {
        return String.valueOf(this.count); //NOPMD
    }

    public BigInteger bigIntegerValue()
    {
        return this.count;
    }
}
