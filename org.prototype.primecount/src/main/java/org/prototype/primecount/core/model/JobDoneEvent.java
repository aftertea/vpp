package org.prototype.primecount.core.model;

import org.prototype.primecount.foundation.AbstractEvent;

public final class JobDoneEvent extends AbstractEvent
{
    public JobDoneEvent() //NOPMD
    {
        super();
    }
}
