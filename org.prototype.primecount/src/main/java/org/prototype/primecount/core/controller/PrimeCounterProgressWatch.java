package org.prototype.primecount.core.controller;

import org.prototype.primecount.core.model.JobDoneEvent;
import org.prototype.primecount.core.model.JobHasProgressedEvent;
import org.prototype.primecount.core.model.JobJustStartedEvent;
import org.prototype.primecount.foundation.AbstractProgressWatch;
import org.prototype.primecount.foundation.Job;
import org.prototype.primecount.foundation.SimpleEventManager;

final class PrimeCounterProgressWatch extends AbstractProgressWatch
{
    protected PrimeCounterProgressWatch()
    {
        super();
    }

    @Override
    public void jobProgressed(final Job job)
    {
        SimpleEventManager.getInstance().dispatch(new JobHasProgressedEvent(job));
    }

    @Override
    public void jobJustStartd()
    {
        SimpleEventManager.getInstance().dispatch(new JobJustStartedEvent());
    }

    @Override
    public void done()
    {
        SimpleEventManager.getInstance().dispatch(new JobDoneEvent());
    }
}
