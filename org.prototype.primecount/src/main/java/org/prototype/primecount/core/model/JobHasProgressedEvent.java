package org.prototype.primecount.core.model;

import org.prototype.primecount.foundation.AbstractEvent;
import org.prototype.primecount.foundation.Job;

public final class JobHasProgressedEvent extends AbstractEvent
{
    private final Job job;

    public JobHasProgressedEvent(final Job jobP)
    {
        super();

        assert null != jobP : "Parameter 'jobP' of 'JobHasProgressedEvent''s ctor must not be null";

        this.job = jobP;
    }

    public Job getJob()
    {
        return this.job;
    }
}
