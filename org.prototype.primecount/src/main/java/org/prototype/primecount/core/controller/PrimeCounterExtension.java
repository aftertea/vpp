package org.prototype.primecount.core.controller;

import java.lang.Thread.UncaughtExceptionHandler;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.ConcurrentHashSet;
import org.prototype.primecount.core.controller.PrimeCounterAdapter.ICoordinator;
import org.prototype.primecount.core.model.IController;
import org.prototype.primecount.foundation.BigIntegerCounter;
import org.prototype.primecount.foundation.IProgressWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class PrimeCounterExtension implements ThreadFactory, UncaughtExceptionHandler, ICoordinator
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PrimeCounterExtension.class);
    private static final String FEATURE_NAME = "Prime Counter";
    private static final int NIGHTY_NINE = 99;
    private static final int ZERO = 0;

    private final BigIntegerCounter primes;
    private final BigIntegerCounter notPrimes;

    private final List<PrimeCounterAdapter> currentlyRunning;
    private final PrototypePrimeController primeController;
    private final int nCores;
    private final int lowerBound;
    private final int upperBound;
    private final ConcurrentHashSet<BigInteger> primeSet;
    private final IProgressWatch progressWatch;

    protected PrimeCounterExtension(final int lBound, final int uBound, final IController controller)
    {
        assert 0 < lBound : "Parameter 'lBound' of 'PrimeCounterExtension''s ctor must be greater than 0";
        assert 0 < uBound : "Parameter 'uBound' of 'PrimeCounterExtension''s ctor must be greater than 0";
        assert lBound < uBound : "Parameter 'upperBound' of 'PrimeCounterExtension''s ctor must be greater than parameter 'lowerBound'";
        assert null != controller : "Parameter 'controller' of 'PrimeCounterExtension''s ctor must not be null";

        this.primeController = (PrototypePrimeController) controller;
        this.primes = new BigIntegerCounter();
        this.notPrimes = new BigIntegerCounter();
        this.lowerBound = lBound;
        this.upperBound = uBound;
        this.primeSet = new ConcurrentHashSet<>();
        this.nCores = Runtime.getRuntime().availableProcessors() - 2;

        assert ZERO < this.nCores : "Field 'nCores' must be greater than 0";

        this.currentlyRunning = new ArrayList<>(this.nCores);
        this.progressWatch = new PrimeCounterProgressWatch();
        this.progressWatch.defineSteps(defineSteps(this.nCores)); //NOPMD
    }

    void run() //NOPMD
    {
        addAdequateRunners();

        assert null != this.currentlyRunning : "Local variable 'currentlyRunning' must not be null";
        assert ZERO < this.currentlyRunning.size() : "Local variable 'currentlyRunning' must not be empty";

        final ExecutorService executorService = Executors.newFixedThreadPool(this.currentlyRunning.size(), this); //NOPMD
        final double startTime = System.currentTimeMillis();
        this.progressWatch.start(FEATURE_NAME);
        synchronized (this.currentlyRunning)
        {
            this.currentlyRunning.forEach(next -> { //NOPMD
                        executorService.submit(next); //NOPMD
                    });
        }
        executorService.shutdown(); //NOPMD
        try
        {
            executorService.awaitTermination(30, TimeUnit.MINUTES); //NOPMD
            final double elapsedTime = System.currentTimeMillis() - startTime;
            this.primeController.consumePrimeCountOutcome(this.primeSet, this.primes, this.notPrimes, this.lowerBound, elapsedTime); //NOPMD
            /** this.primeSet pass not sorted to save time*/
            this.progressWatch.done();
        }
        catch (InterruptedException e)
        {
            LOGGER.error("in method 'run'", e);
        }
    }

    private static int[] defineSteps(final int nCores)
    {
        assert ZERO < nCores : "Parameter 'nCores' of method 'defineSteps' must be greater than 0";

        final int weight = NIGHTY_NINE / nCores;
        final int[] weights = new int[nCores];
        Arrays.fill(weights, weight);
        return weights;
    }

    private void addAdequateRunners()
    {
        assert null != this.currentlyRunning : "'currentlyRunning' must not be null";
        assert ZERO < this.lowerBound : "'lowerBound' must be greater than 0";
        assert ZERO < this.upperBound : "'upperBound' must be greater than 0";
        assert this.lowerBound <= this.upperBound : "'upperBound' must be greater than parameter 'lowerBound'";

        final int chunkSize = this.upperBound / this.nCores;
        this.currentlyRunning.add(new PrimeCounterAdapter(this, this.lowerBound, chunkSize, this.primeSet, this.primes, this.notPrimes));
        int nextThreadNo = 1;
        /**No concurrent access to this map*/
        final Map<Integer, Integer> lower2UpperBounds = new HashMap<>(this.nCores); //NOPMD 
        for (; nextThreadNo < this.nCores - 1; nextThreadNo++)
        {
            final int nextLowerBound = chunkSize * nextThreadNo + 1;
            final int nextUpperBound = chunkSize * (nextThreadNo + 1);
            lower2UpperBounds.put(Integer.valueOf(nextLowerBound), Integer.valueOf(nextUpperBound)); //NOPMD
        }
        lower2UpperBounds.entrySet().forEach(next -> {
            this.currentlyRunning.add(new PrimeCounterAdapter(this, next.getKey(), next.getValue().intValue(), this.primeSet, this.primes, //NOPMD
                    this.notPrimes)); //NOPMD
            });
        if (nextThreadNo < this.nCores) //TODO we do not need this check anyway - consider remove
        {
            final int lastLowerBound = chunkSize * nextThreadNo + 1;
            final int remainder = this.upperBound % this.nCores;
            final int lastUpperBound = chunkSize * (nextThreadNo + 1) + remainder;
            this.currentlyRunning.add(new PrimeCounterAdapter(this, lastLowerBound, lastUpperBound, this.primeSet, this.primes, this.notPrimes));
        }
    }

    @Override
    public Thread newThread(final Runnable runnablePassed)
    {
        assert null != runnablePassed : "Parameter 'runnablePassed' of method 'newThread' must not be null";

        final Thread thread = new Thread(runnablePassed);
        thread.setName(FEATURE_NAME);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.setUncaughtExceptionHandler(this);
        return thread;
    }

    @Override
    public void uncaughtException(final Thread thread, final Throwable exeption)
    {
        assert null != thread : "Parameter 'thread' of method 'uncaughtException' must not be null";
        assert null != exeption : "Parameter 'exeption' of method 'uncaughtException' must not be null";

        LOGGER.error("UnchaughtException has been caughted", exeption);
    }

    @Override
    public IProgressWatch provideProgressWatch()
    {
        return this.progressWatch;
    }
}
