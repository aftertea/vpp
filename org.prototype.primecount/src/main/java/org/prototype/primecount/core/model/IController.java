package org.prototype.primecount.core.model;

public interface IController
{
    PrototypePrimeOutcome getPrimeCountOutcome();

    void featureOutcomeReady();

    void consumeUpperBound(final int value);

    int getUpperBound();

    void clearCurrentInputData();

    String getPrimeCountOutcomeValue();
}
