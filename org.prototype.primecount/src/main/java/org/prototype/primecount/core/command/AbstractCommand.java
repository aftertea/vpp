package org.prototype.primecount.core.command;

public abstract class AbstractCommand
{
    private final String commandId;

    protected AbstractCommand(final String idPassed)
    {
        super();
        this.commandId = idPassed;
    }

    public String getId()
    {
        return this.commandId;
    }

    public abstract void run();
}
