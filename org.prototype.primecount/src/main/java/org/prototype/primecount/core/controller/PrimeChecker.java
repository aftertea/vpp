package org.prototype.primecount.core.controller;

final class PrimeChecker
{
    private static final int FIRST_PRIME = 2;
    private static final int ZERO = 0;

    private PrimeChecker()
    {
        super();
    }

    public static boolean isPrime(final int integer)
    {
        boolean prime = true; //NOPMD
        if (isIntegerLessThanFirstPrime(integer))
        {
            prime = false;
        }
        else if (isIntegerEqualsFirstPrime(integer)) /**anyway no need for this check as prime is set to true*/
        {
            prime = true;
        }
        else
        {
            final int max = (int) Math.ceil(Math.sqrt(integer));
            int nextInt = FIRST_PRIME;
            while (nextInt <= max)
            {
                if (isThisModoluThatEqualsZero(integer, nextInt))
                {
                    prime = false;
                    break;
                }
                nextInt++;
            }
        }
        return prime;
    }

    private static boolean isThisModoluThatEqualsZero(final int integer, final int that)
    {
        return ZERO == integer % that;
    }

    private static boolean isIntegerEqualsFirstPrime(final int integer)
    {
        return FIRST_PRIME == integer;
    }

    private static boolean isIntegerLessThanFirstPrime(final int integer)
    {
        return FIRST_PRIME > integer;
    }
}
