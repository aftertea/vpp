package org.prototype.primecount.core.command;

import org.prototype.primecount.core.controller.PrototypePrimeController;
import org.prototype.primecount.core.model.IController;

public final class ProtoPrimeControllerCreator
{
    private ProtoPrimeControllerCreator()
    {
        super();
    }

    public static IController create()
    {
        return new PrototypePrimeController();
    }
}
