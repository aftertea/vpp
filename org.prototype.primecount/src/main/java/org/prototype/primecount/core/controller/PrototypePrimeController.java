package org.prototype.primecount.core.controller;

import java.math.BigInteger;
import java.util.Collection;

import org.prototype.primecount.core.model.IController;
import org.prototype.primecount.core.model.PrimeCountResultReady;
import org.prototype.primecount.core.model.PrototypePrimeOutcome;
import org.prototype.primecount.foundation.BigIntegerCounter;
import org.prototype.primecount.foundation.SimpleEventManager;

public final class PrototypePrimeController implements IController
{
    private final PrototypePrimeOutcome outcome;

    public PrototypePrimeController()
    {
        this.outcome = new PrototypePrimeOutcome();
    }

    @Override
    public PrototypePrimeOutcome getPrimeCountOutcome()
    {
        return this.outcome;
    }

    public void startCountPrimes()
    {
        new UserStoryFeatureExtension(this).run();
    }

    @Override
    public void featureOutcomeReady()
    {
        SimpleEventManager.getInstance().dispatch(new PrimeCountResultReady(this));
    }

    @Override
    public void consumeUpperBound(final int value)
    {
        this.outcome.consumeUpperBound(value);
    }

    @Override
    public int getUpperBound()
    {
        return this.outcome.getUpperBound();
    }

    @Override
    public void clearCurrentInputData()
    {
        this.outcome.resetModel();
    }

    @Override
    public String getPrimeCountOutcomeValue()
    {
        return this.outcome.getPrimeCountStringValue();
    }

    public void consumePrimeCountOutcome(final Collection<BigInteger> primeSet, final BigIntegerCounter primes, final BigIntegerCounter notPrimes,
            final int lowerBound, final double elapsedTime)
    {
        this.outcome.consumePrimeCountOutcome(primeSet, primes, notPrimes, lowerBound, elapsedTime);
    }
}
