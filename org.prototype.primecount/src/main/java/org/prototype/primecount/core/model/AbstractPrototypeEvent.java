package org.prototype.primecount.core.model;

import org.prototype.primecount.foundation.AbstractEvent;

public abstract class AbstractPrototypeEvent extends AbstractEvent
{
    private final IController controller;

    protected AbstractPrototypeEvent(final IController iController)
    {
        super();

        assert null != iController : "Parameter 'iController' of 'AbstractPrototypeEvent''s ctor must not be null";

        this.controller = iController;
    }

    public final IController getController()
    {
        return this.controller;
    }
}
