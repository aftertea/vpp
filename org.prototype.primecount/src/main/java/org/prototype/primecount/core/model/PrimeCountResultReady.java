package org.prototype.primecount.core.model;

public final class PrimeCountResultReady extends AbstractPrototypeEvent
{
    public PrimeCountResultReady(final IController iController)
    {
        super(iController);
    }
}
