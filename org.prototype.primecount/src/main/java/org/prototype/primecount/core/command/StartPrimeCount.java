package org.prototype.primecount.core.command;

import org.prototype.primecount.core.controller.PrototypePrimeController;
import org.prototype.primecount.core.model.IController;

public final class StartPrimeCount extends AbstractCommand
{
    private final static String COMMAND_ID = "StartPrimeCount";
    private final PrototypePrimeController controller;

    public StartPrimeCount(final IController iController)
    {
        super(COMMAND_ID);

        assert null != iController : "Parameter 'iController' of 'StartPrimeCount''s ctor must not be null";

        this.controller = (PrototypePrimeController) iController;
    }

    @Override
    public void run()
    {
        this.controller.startCountPrimes();
    }
}
