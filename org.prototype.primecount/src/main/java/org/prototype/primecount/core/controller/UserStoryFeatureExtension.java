package org.prototype.primecount.core.controller;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.prototype.primecount.core.model.IController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserStoryFeatureExtension implements ThreadFactory, UncaughtExceptionHandler
{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserStoryFeatureExtension.class);

    private final IController controller;

    public UserStoryFeatureExtension(final IController iController)
    {
        assert null != iController : "Parameter 'iController' of method 'UserStoryFeatureExtension' must not be null";

        this.controller = iController;
    }

    public void run()
    {
        final ExecutorService executorService = Executors.newFixedThreadPool(1, this);
        executorService.submit(new StartFeatureAdapter(this.controller));
    }

    @Override
    public Thread newThread(final Runnable adapter)
    {
        assert null != adapter : "Parameter 'adapter' of method 'newThread' must not be null";

        final Thread thread = new Thread(adapter);
        thread.setName("Feature Adapter");
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.setUncaughtExceptionHandler(this);
        return thread;
    }

    @Override
    public void uncaughtException(final Thread thread, final Throwable exception)
    {
        LOGGER.error("uncaughtException has been caughted", exception);
    }
}
