package org.prototype.primecount.core.controller;

import java.math.BigInteger;

import org.eclipse.jetty.util.ConcurrentHashSet;
import org.prototype.primecount.foundation.BigIntegerCounter;
import org.prototype.primecount.foundation.IProgressWatch;

public final class PrimeCounterAdapter implements Runnable
{
    private final ICoordinator coordinator;
    private final int adapterLBound;
    private final int adapterUBound;
    private final ConcurrentHashSet<BigInteger> primeSetRef;
    private final BigIntegerCounter primes;
    private final BigIntegerCounter notPrimes;

    public PrimeCounterAdapter(final ICoordinator extension, final int lBoundPassed, final int uBoundPassed,
            final ConcurrentHashSet<BigInteger> primeSetPassed, final BigIntegerCounter prime, final BigIntegerCounter notprime)
    {
        assert null != extension : "Parameter 'extension' of 'PrimeCounterAdapter''s ctor must not be null";
        assert null != primeSetPassed : "Parameter 'primeSetPassed' of 'PrimeCounterAdapter''s ctor must not be null";

        this.coordinator = extension;
        this.adapterLBound = lBoundPassed;
        this.adapterUBound = uBoundPassed;
        this.primeSetRef = primeSetPassed;
        this.primes = prime;
        this.notPrimes = notprime;
    }

    @Override
    public void run()
    {
        int nextInteger = this.adapterLBound;
        while (nextInteger <= this.adapterUBound)
        {
            if (PrimeChecker.isPrime(nextInteger))
            {
                this.primeSetRef.add(BigInteger.valueOf(nextInteger));
                this.primes.increment();
            }
            else
            {
                this.notPrimes.increment();
            }
            nextInteger++;
        }
        this.coordinator.provideProgressWatch().endStep();
    }

    public interface IStepTracker
    {
        void whenLaunch();

        void whenFinished();

        void whenFailed();

        void progressFurther(final float step);

        void hasBeenStopped();
    }

    interface ICoordinator
    {
        IProgressWatch provideProgressWatch();
    }
}