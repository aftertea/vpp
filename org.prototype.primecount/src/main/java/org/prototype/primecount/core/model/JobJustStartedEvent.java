package org.prototype.primecount.core.model;

import org.prototype.primecount.foundation.AbstractEvent;

public final class JobJustStartedEvent extends AbstractEvent
{
    public JobJustStartedEvent() //NOPMD
    {
        super();
    }
}
