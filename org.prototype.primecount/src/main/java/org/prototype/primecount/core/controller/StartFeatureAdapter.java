package org.prototype.primecount.core.controller;

import org.prototype.primecount.core.model.IController;

public final class StartFeatureAdapter implements Runnable
{
    private final IController controller;

    public interface IWorkProgressTracker
    {
        void start();

        void advance();

        void stop();
    }

    public StartFeatureAdapter(final IController iController)
    {
        assert null != iController : "Parameter 'iController' of method 'StartFeatureAdapter' must not be null";

        this.controller = iController;
    }

    @Override
    public void run()
    {
        new PrimeCounterExtension(1, this.controller.getUpperBound(), this.controller).run();
        this.controller.featureOutcomeReady();
    }
}
