package org.prototype.primecount.core.model;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.prototype.primecount.foundation.BigIntegerCounter;
import org.prototype.primecount.foundation.Constants;

public final class PrototypePrimeOutcome
{
    private int upperBound;
    private String primeCountValue;
    private Set<BigInteger> primeSet;
    private BigInteger primeCount;
    private BigInteger notPrimeCount;
    private double timeCost;
    private int lowerBound;

    public PrototypePrimeOutcome() //NOPMD
    {
        super();
    }

    public void consumeUpperBound(final int valueTyped)
    {
        this.upperBound = valueTyped;
    }

    public int getUpperBound()
    {
        return this.upperBound;
    }

    public void consumePrimeCountResult(final Set<BigInteger> primeSetRef, final String primeCountOutput)
    {
        assert null != primeSetRef : "Parameter 'primeSetRef' of method 'consumePrimeCountResult' must not be null";
        assert null != primeCountOutput && !primeCountOutput.isEmpty() : "Parameter 'primeCountOutput' of method 'consumePrimeCountResult' must not be empty";

        this.primeSet = primeSetRef;
        this.primeCountValue = primeCountOutput;
    }

    public String getPrimeCountStringValue()
    {
        return this.primeCountValue;
    }

    public void resetModel()
    {
        this.upperBound = 0;
        this.primeCountValue = Constants.EMPTY;
    }

    public Set<BigInteger> getPrimeSet()
    {
        return Collections.unmodifiableSet(this.primeSet); //NOPMD
    }

    public void consumePrimeCountOutcome(final Collection<BigInteger> primeSetCollected, final BigIntegerCounter primes,
            final BigIntegerCounter notPrimes, final int lBound, final double elapsedTime)
    {
        assert null != primeSetCollected : "Parameter 'primeSetCollected' of method 'consumePrimeCountOutcome' must not be null";
        assert null != notPrimes : "Parameter 'notPrimes' of method 'consumePrimeCountOutcome' must not be null";
        assert null != primes : "Parameter 'primes' of method 'consumePrimeCountOutcome' must not be null";

        this.primeSet = (Set<BigInteger>) primeSetCollected;
        this.primeCount = primes.bigIntegerValue();
        this.notPrimeCount = notPrimes.bigIntegerValue();
        this.primeCountValue = this.toString();
        this.timeCost = elapsedTime;
        this.lowerBound = lBound;
    }

    @Override
    public String toString()
    {
        final String LINE_SEPARATOR = System.getProperty("line.separator");
        final StringBuilder strBldr = new StringBuilder(124);
        strBldr.append("\t[-] Amount of Primes between ").append(this.lowerBound).append(" and ").append(this.upperBound).append(" is ")
                .append(this.primeCount).append(LINE_SEPARATOR).append(" \t[-] Amount of not-Primes between ").append(this.lowerBound)
                .append(" and ").append(this.upperBound).append(" is ").append(this.notPrimeCount).append(LINE_SEPARATOR).append(LINE_SEPARATOR)
                .append(" \t[-] Time elapsed: ").append(this.timeCost).append(" milli seconds");
        strBldr.trimToSize();
        return strBldr.toString();
    }

    public BigInteger getPrimeCount()
    {
        return this.primeCount;
    }
}
