package org.prototype.primecount.view;

import java.util.HashSet;
import java.util.Set;

import org.prototype.primecount.foundation.Constants;
import org.prototype.primecount.view.common.ComponentOperations;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public final class OutcomeOfUserStory extends AbstractPrototypeComponent
{
    private final TextArea txta4Result;
    private final Set<AbstractField<?>> fieldSet;

    public OutcomeOfUserStory(final AbstractPrototypeComponent parent)
    {
        super(parent, new VerticalLayout());
        this.txta4Result = new TextArea("Ouput");
        this.fieldSet = new HashSet<>(2);
        this.fieldSet.add(this.txta4Result);
        defineWithin(this.layout);
    }

    @Override
    protected void defineWithin(final AbstractLayout container)
    {
        this.txta4Result.setEnabled(false);
        this.txta4Result.setSizeFull();
        setSizeFull();
        ComponentOperations.addComponents(container, this.txta4Result); //NOPMD
    }

    @Override
    public void reset()
    {
        this.fieldSet.forEach(next -> {

            reset(next);

        });
    }

    private void reset(final AbstractField<?> next)
    {
        assert null != next : "Parameter 'next' of method 'reset' must not be null";

        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access(() -> (resetAbstractField(next))); //NOPMD
        }
    }
    
    private void resetAbstractField(final AbstractField<?> next)
    {
        if (next instanceof AbstractTextField)
        {
            ((AbstractTextField) next).setValue(Constants.EMPTY);
        }
        else
        {
            next.setValue(null);
        }
    }
    
    public void setoutput(final String primeCountRslt)
    {
        assert null != primeCountRslt && !primeCountRslt.isEmpty() : "Parameter 'primeCountRslt' of method 'setoutput' must not be empty";

        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access(() -> this.txta4Result.setValue(primeCountRslt)); //NOPMD
        }
    }

    public interface IVisitor extends IPrototypeComponentVisitor
    {
        void visitOutcomeUserStory();
    }

    @Override
    public void accept(final IPrototypeComponentVisitor visitor)
    {
        if (visitor instanceof IVisitor)
        {
            ((IVisitor) visitor).visitOutcomeUserStory(); //NOPMD
        }
        else
        {
            super.accept(visitor);
        }
    }
}
