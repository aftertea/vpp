package org.prototype.primecount.view.handler;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public final class DefaultClickListener implements ClickListener
{
    public enum ButtonHandler
    {
        BUTTON_START(new PrimeCountAttemptStart()), 
        BUTTON_STOP(new AttemptStop());

        private final AbstractViewCommandHandler handler;

        private ButtonHandler(final AbstractViewCommandHandler vHandler)
        {
            this.handler = vHandler;
        }

        AbstractViewCommandHandler getHandler()
        {
            return this.handler;
        }
    }

    private final ButtonHandler buttonHandler;

    public DefaultClickListener(final ButtonHandler button)
    {
        assert null != button : "Parameter 'button' of 'DefaultClickListener''s ctor must not be null";

        this.buttonHandler = button;
    }

    @Override
    public void buttonClick(final ClickEvent event)
    {
        this.buttonHandler.getHandler().execute(event);
    }
}
