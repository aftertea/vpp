package org.prototype.primecount.view;

import org.prototype.primecount.foundation.HeapStatusSimpleProvider;
import org.prototype.primecount.view.common.ComponentOperations;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public final class EnvironmentStatusbar extends AbstractPrototypeComponent
{
    private final Label labelReady;
    private final Label labelHeap;
    protected final TextField txtfHeapStts; //NOPMD
    private final TextField txtfPlatform;

    public EnvironmentStatusbar(final AbstractPrototypeComponent parent)
    {
        super(parent, new VerticalLayout());

        this.labelReady = new Label("Ready");
        this.txtfHeapStts = new TextField();
        this.txtfHeapStts.setEnabled(false);
        this.labelHeap = new Label();
        this.txtfPlatform = new TextField();
        defineWithin(super.layout);
    }

    @Override
    protected void defineWithin(final AbstractLayout container)
    {
        final StringBuilder heapInfo = composeHeapInfo();
        this.labelHeap.setValue(heapInfo.toString()); //NOPMD

        //TODO this is a niive - problematic submitting a thread 
        //consider use of ExecutorService API.
        //And also take it out from here
        final Thread heapSttsRfrshThrd = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (true) //TODO pick up different check
                {
                    final String heapStatus = HeapStatusSimpleProvider.provideHeapStatus();
                    amendHeapStatusField(EnvironmentStatusbar.this.txtfHeapStts, heapStatus); //NOPMD
                    LoggerFactory.getLogger(Runnable.class).info("-> Heap size: '{}'", heapStatus); //NOPMD
                    try
                    {
                        Thread.sleep(20 * 1000);
                    }
                    catch (InterruptedException e)
                    {
                        LoggerFactory.getLogger(Runnable.class).error("Error in mehtod 'run' ", e); //NOPMD
                    }
                }
            }
        });
        heapSttsRfrshThrd.start();

        final StringBuilder platformInfo = composePlatformInfo();

        this.txtfPlatform.setValue(platformInfo.toString()); //NOPMD
        this.txtfPlatform.setEnabled(false);
        this.txtfPlatform.setSizeFull();
        setSizeFull();
        final HorizontalLayout statusBar = new HorizontalLayout();
        statusBar.setSizeFull();
        statusBar.addComponents(this.labelReady, this.labelHeap, this.txtfHeapStts); //NOPMD
        statusBar.setExpandRatio(this.labelReady, 44.0f); //NOPMD
        statusBar.setExpandRatio(this.labelHeap, 10.0f); //NOPMD
        statusBar.setComponentAlignment(this.labelHeap, Alignment.MIDDLE_CENTER); //NOPMD
        ComponentOperations.addComponents(container, statusBar, this.txtfPlatform); //NOPMD
    }

    //TODO move this to a foundation class
    private static StringBuilder composeHeapInfo()
    {
        final StringBuilder heapInfo = new StringBuilder();
        heapInfo.append("VM (").append(System.getProperty("java.vm.info")).append("): Heap ");
        heapInfo.trimToSize();
        return heapInfo;
    }

    //TODO move this to a foundation class
    private static StringBuilder composePlatformInfo()
    {
        final StringBuilder platformInfo = new StringBuilder(40);
        platformInfo.append(System.getProperty("os.name")).append(" (").append(System.getProperty("os.version")).append(") \t ")
                .append(System.getProperty("os.arch").replaceAll("amd", "").concat(" bit"))
                //NOPMD
                .append(" \t ").append(Runtime.getRuntime().availableProcessors())
                //NOPMD
                .append(" Cores \t").append(Runtime.class.getPackage().getImplementationTitle())
                //NOPMD
                .append(" - ").append(System.getProperty("java.version")).append(" \t ").append(System.getProperty("java.vendor"))
                .append(" \t Language ").append(System.getProperty("user.language")); //NOPMD
        platformInfo.trimToSize();
        return platformInfo;
    }

    protected static void amendHeapStatusField(final TextField txtfHeapSttus, final String formatter)
    {
        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access( //NOPMD
                    () -> {
                        if (null == formatter || null == txtfHeapSttus)
                        {
                            LoggerFactory.getLogger(EnvironmentStatusbar.class).warn( //NOPMD
                                    "'formatter' and 'txtfHeapStts' of method 'amendHeapStatusField' both must no be null");
                            return;
                        }
                        txtfHeapSttus.setValue(formatter);
                    }); //NOPMD
        }
    }

    @Override
    public void reset()
    {
        /** nothing to do so far */
    }
}
