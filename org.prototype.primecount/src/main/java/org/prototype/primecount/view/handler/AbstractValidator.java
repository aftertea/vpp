package org.prototype.primecount.view.handler;

import java.util.Locale;

import org.prototype.primecount.foundation.ProtoInvalidValueException;
import org.prototype.primecount.view.base.SimplePrototypeRegistry;

import com.vaadin.data.Validator;
import com.vaadin.data.util.converter.AbstractStringToNumberConverter;
import com.vaadin.data.util.converter.Converter.ConversionException;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

@SuppressWarnings("serial")
public abstract class AbstractValidator<T> implements Validator
{
    private static final String DEF_MSG = "Unexpected value";

    private final AbstractStringToNumberConverter<T> string2NumCnvrtr;
    private final String vldtMessage;

    protected AbstractValidator(final String validateMessage, final AbstractStringToNumberConverter<T> converterPassed)
    {
        assert null != converterPassed : "Parameter 'converterPassed' of 'AbstractValidator''s ctor must not be null";

        this.string2NumCnvrtr = converterPassed;

        if (null == validateMessage || validateMessage.isEmpty())
        {
            this.vldtMessage = DEF_MSG;
        }
        else
        {
            this.vldtMessage = validateMessage;
        }
    }

    @Override
    public void validate(final Object value) throws InvalidValueException
    {
        assert null != value : "Parameter 'value' of method 'validate' must not be null";

        if (value instanceof String)
        {
            try
            {
                SimplePrototypeRegistry.getInstance().getController().clearCurrentInputData(); //NOPMD
                final Class<? extends T> targetType = this.string2NumCnvrtr.getModelType();
                final Locale locale = Locale.UK;
                final Integer sourceValue = (Integer) this.string2NumCnvrtr.convertToModel((String) value, targetType, locale);
                SimplePrototypeRegistry.getInstance().getController().consumeUpperBound(sourceValue); //NOPMD
            }
            catch (ConversionException e)
            {
                //TODO consider moving this notification invokation to a better place
                Notification.show("Validation", this.vldtMessage, Type.ERROR_MESSAGE); //NOPMD

                throw new ProtoInvalidValueException(this.vldtMessage, e);
            }
        }
    }
}
