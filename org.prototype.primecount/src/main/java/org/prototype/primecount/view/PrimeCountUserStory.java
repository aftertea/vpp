package org.prototype.primecount.view;

import org.prototype.primecount.core.model.PrimeCountResultReady;
import org.prototype.primecount.foundation.AbstractEventHandler;
import org.prototype.primecount.foundation.SimpleEventManager;
import org.prototype.primecount.view.base.UserStoryCleaner;
import org.prototype.primecount.view.base.UserStoryOutputFeeder;
import org.prototype.primecount.view.common.ComponentOperations;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.AbstractLayout;

@SuppressWarnings("serial")
public final class PrimeCountUserStory extends AbstractUserStoryProgressive
{
    private static final String CAPTION = "Prime Count";

    private final OutcomeOfUserStory outcomeUserStory;
    private final InUserStory inUserStory;
    private final UserStoryToolbar userStoryToolbar;

    public PrimeCountUserStory(final AbstractComponent vaadinParent)
    {
        super(vaadinParent, null, CAPTION);

        this.outcomeUserStory = new OutcomeOfUserStory(this);
        this.userStoryToolbar = new UserStoryToolbar(this);
        this.inUserStory = new InUserStory(this, new UserStoryCleaner(this));
        defineWithin(super.layout);

        SimpleEventManager.getInstance().attach(new AbstractEventHandler<PrimeCountResultReady>(PrimeCountResultReady.class)
        {
            @Override
            public void handle(final PrimeCountResultReady event)
            {
                assert null != event : "Parameter 'event' of method 'handle' must not be null";

                final IPrototypeComponentVisitor visitor = new UserStoryOutputFeeder(event.getController());
                accept(visitor);
            }
        });
    }

    @Override
    protected void defineWithin(final AbstractLayout container)
    {
        super.defineWithin(container);

        ComponentOperations.addComponents(container, this.inUserStory, this.userStoryToolbar, this.outcomeUserStory, new EnvironmentStatusbar(this));
    }

    public interface IVisitor extends IPrototypeComponentVisitor
    {
        void visitOutcomeUserStory(final OutcomeOfUserStory userStoryResult);
    }

    @Override
    public void accept(final IPrototypeComponentVisitor visitor)
    {
        if (visitor instanceof IVisitor)
        {
            ((IVisitor) visitor).visitOutcomeUserStory(this.outcomeUserStory); //NOPMD
        }
    }

    @Override
    public void reset()
    {
        this.layout.iterator().forEachRemaining(nextComponent -> { //NOPMD
            if (nextComponent instanceof AbstractPrototypeComponent)
            {
                ((AbstractPrototypeComponent) nextComponent).reset(); //NOPMD
            }
        });
    }
}
