package org.prototype.primecount.view;

import org.prototype.primecount.view.common.ComponentOperations;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public abstract class AbstractPrototypeComponent extends CustomComponent
{
    private final AbstractPrototypeComponent parent;
    protected final AbstractLayout layout;

    protected AbstractPrototypeComponent(final AbstractPrototypeComponent component, final AbstractLayout container)
    {
        super();

        if (null == component)
        {
            this.parent = this;
        }
        else
        {
            this.parent = component;
        }
        this.layout = container;
        ComponentOperations.establishLayout(this, this.layout);
        setCompositionRoot(this.layout);
    }

    protected abstract void defineWithin(final AbstractLayout container);

    protected final AbstractPrototypeComponent getGenuineParent()
    {
        return this.parent;
    }

    public interface IPrototypeComponentVisitor
    {
        void visitPrototypeComponent(final AbstractPrototypeComponent component);
    }

    public void accept(final IPrototypeComponentVisitor visitor)
    {
        assert null != visitor : "Parameter 'visitor' of method 'accept' must not be null";

        visitor.visitPrototypeComponent(this);
    }

    public abstract void reset();
}
