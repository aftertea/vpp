package org.prototype.primecount.view;

import org.prototype.primecount.core.controller.PrimeCounterAdapter.IStepTracker;
import org.prototype.primecount.core.model.JobDoneEvent;
import org.prototype.primecount.core.model.JobHasProgressedEvent;
import org.prototype.primecount.core.model.JobJustStartedEvent;
import org.prototype.primecount.foundation.AbstractEventHandler;
import org.prototype.primecount.foundation.Constants;
import org.prototype.primecount.foundation.SimpleEventManager;
import org.prototype.primecount.view.common.ComponentOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
final class UIWorkProgressTracker extends AbstractPrototypeComponent implements IStepTracker
{ //NOPMD
    private static final Logger LOGGER = LoggerFactory.getLogger(UIWorkProgressTracker.class);
    private static final float INITIAL = 0.0F;
    private static final float STEP = 0.1F;
    private static final float FINISHED = 1.0F;
    private static final int ONE = 1;
    private static final int HUNDRED = 100;

    private final ProgressBar progressor;
    private final Label status;

    protected UIWorkProgressTracker(final AbstractPrototypeComponent parent)
    {
        super(parent, new HorizontalLayout());

        this.progressor = new ProgressBar();
        this.status = new Label();
        setSizeFull();
        defineWithin(super.layout);

        SimpleEventManager.getInstance().attach(new JobProgressHandler(JobHasProgressedEvent.class));

        SimpleEventManager.getInstance().attach(new JobJustStartedHandler(JobJustStartedEvent.class));

        SimpleEventManager.getInstance().attach(new JobDoneHandler(JobDoneEvent.class));
    }

    @Override
    protected void defineWithin(final AbstractLayout container)
    {
        ComponentOperations.addComponents(container, this.progressor, this.status); //NOPMD
        this.progressor.setSizeFull();
        this.status.setValue(Constants.HAS_NOT_STARTED);
    }

    @Override
    public void whenLaunch()
    {
        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access(() -> whenLaunchInternal()); //NOPMD
        }
    }

    private void whenLaunchInternal()
    {
        this.progressor.setValue(Float.valueOf(STEP));
        this.status.setValue(Constants.IN_PROGRESS);
    }

    @Override
    public void whenFinished()
    {
        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access(() -> doneInternal()); //NOPMD
        }
    }

    private void doneInternal()
    {
        this.progressor.setValue(Float.valueOf(FINISHED));
        this.status.setValue(Constants.DONE);
    }

    @Override
    public void whenFailed()
    {
        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access(() -> whenFailedInternal()); //NOPMD
        }
    }

    private void whenFailedInternal()
    {
        this.progressor.setValue(Float.valueOf(INITIAL));
        this.status.setValue(Constants.HAS_FAILED);
    }

    @Override
    public void progressFurther(final float step)
    {
        final float progressedUntil = updateLogic(step);

        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access(() -> update(progressedUntil)); //NOPMD
        }
    }

    private void update(final float progressedUntil)
    {
        this.progressor.setValue(progressedUntil);
        this.status.setValue(Constants.IN_PROGRESS);
    }

    private float updateLogic(final float step)
    {
        assert INITIAL < step : "A step needs to be greater than 0.0";

        float progressedUntil = Float.valueOf(step); //NOPMD
        float nextStepWeight = STEP; //NOPMD
        if (step > ONE)
        {
            progressedUntil = Float.valueOf(step) / HUNDRED;
        }
        if (ONE < progressedUntil)
        {
            LOGGER.debug("'progressedUntil' of method 'update' must be less than 1");
        }
        if (STEP > progressedUntil)
        {
            LOGGER.debug("'progressedUntil' of method 'update' must be greater than 0.1");
        }
        if (STEP > progressedUntil)
        {
            nextStepWeight = STEP; //NOPMD
        }
        final float soFar = this.progressor.getValue();
        if (progressedUntil > soFar)
        {
            final int progressFromHere = (int) (soFar * 100 + 1);
            final int untilHere = Float.valueOf(progressedUntil * 100).intValue(); //NOPMD
            if (1 > progressFromHere || 100 < untilHere)
            {
                nextStepWeight = STEP;
            }
            else
            {
                nextStepWeight = progressedUntil;
            }
        }
        return nextStepWeight;
    }

    @Override
    public void hasBeenStopped()
    {
        /** TODO will be implemented with the user feature 'attempt stop' */
    }

    public void reset()
    {
        this.progressor.setValue(INITIAL);
        this.status.setValue(Constants.HAS_NOT_STARTED);
    }

    private class JobProgressHandler extends AbstractEventHandler<JobHasProgressedEvent>
    {
        protected JobProgressHandler(final Class<JobHasProgressedEvent> eventClassPassed)
        {
            super(eventClassPassed);
        }

        @Override
        public void handle(final JobHasProgressedEvent event)
        {
            assert null != event : "Parameter 'event' of method 'handle' must not be null";

            progressFurther(event.getJob().howMuchProgressed()); //NOPMD
        }
    }

    private class JobJustStartedHandler extends AbstractEventHandler<JobJustStartedEvent>
    {
        protected JobJustStartedHandler(final Class<JobJustStartedEvent> eventClassPassed)
        {
            super(eventClassPassed);
        }

        @Override
        public void handle(final JobJustStartedEvent event)
        {
            whenLaunch();
        }
    }

    private class JobDoneHandler extends AbstractEventHandler<JobDoneEvent>
    {
        protected JobDoneHandler(final Class<JobDoneEvent> eventClassPassed)
        {
            super(eventClassPassed);
        }

        @Override
        public void handle(final JobDoneEvent event)
        {
            whenFinished();
        }
    }
}
