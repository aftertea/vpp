package org.prototype.primecount.view;

import org.prototype.primecount.view.common.ComponentOperations;
import org.prototype.primecount.view.handler.DefaultClickListener;
import org.prototype.primecount.view.handler.DefaultClickListener.ButtonHandler;

import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;

@SuppressWarnings("serial")
public final class UserStoryToolbar extends AbstractPrototypeComponent
{
    private final static String CAPTION_START = "Attempt Start";
    private final static String CAPTION_STOP = "Attempt Stop";

    private final Button buttonStart;
    private final Button buttonStop;

    public UserStoryToolbar(final AbstractPrototypeComponent parent)
    {
        super(parent, new HorizontalLayout());

        this.buttonStart = new Button(CAPTION_START, new DefaultClickListener(ButtonHandler.BUTTON_START));
        this.buttonStop = new Button(CAPTION_STOP, new DefaultClickListener(ButtonHandler.BUTTON_STOP));
        this.buttonStop.setEnabled(false); //TODO functionality's implementation required
        defineWithin(super.layout);
    }

    @Override
    protected void defineWithin(final AbstractLayout container)
    {
        ComponentOperations.addComponents(container, this.buttonStart, this.buttonStop); //NOPMD
    }

    @Override
    public void reset()
    {
        /**nothing to do so far*/
    }
}
