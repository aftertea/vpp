package org.prototype.primecount.view.handler;

import org.prototype.primecount.view.AbstractPrototypeComponent.IPrototypeComponentVisitor;
import org.prototype.primecount.view.AbstractUserStoryProgressive;
import org.prototype.primecount.view.OutcomeOfUserStory.IVisitor;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

@SuppressWarnings("serial")
public final class DefaultValueChangeListener implements ValueChangeListener
{
    private final IPrototypeComponentVisitor cleanVisitor;

    //TODO remove
    //private String currentValue;

    public DefaultValueChangeListener(final IPrototypeComponentVisitor cleaner)
    {
        super();

        assert null != cleaner : "Parameter 'cleaner' of 'DefaultValueChangeListener''s ctor must not be null";

        this.cleanVisitor = cleaner;
    }

    @Override
    public void valueChange(final ValueChangeEvent event)
    {
        assert null != event : "Parameter 'event' of method 'valueChange' must not be null";

        /** No need for the following logic - Vaadin performs this check implicitly and does not trigger the event unless the new value is different than the current value*/
        /* final String value = (String) event.getProperty().getValue(); //NOPMD
         if (null == this.currentValue)
         {
             this.currentValue = new String(Constants.EMPTY);
         }
         if (null != value && 0 == value.compareTo(this.currentValue))
         {
             return;
         }
         this.currentValue = value;*/
        ((IVisitor) this.cleanVisitor).visitOutcomeUserStory(); //NOPMD
        ((AbstractUserStoryProgressive.IVisitor) this.cleanVisitor).visitUserStoryProgressive();//NOPMD
        
    }
}
