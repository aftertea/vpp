package org.prototype.primecount.view.handler;

import org.prototype.primecount.core.command.StartPrimeCount;

import com.vaadin.ui.Component;

public final class PrimeCountAttemptStart extends AbstractViewCommandHandler
{
    public PrimeCountAttemptStart() //NOPMD
    {
        super();
    }

    @Override
    protected void executeInternal(final Component.Event event)
    {
        new StartPrimeCount(getController()).run();
    }
}
