package org.prototype.primecount.view.handler;

import org.prototype.primecount.core.model.IController;
import org.prototype.primecount.view.base.SimplePrototypeRegistry;

import com.vaadin.ui.Component;

public abstract class AbstractViewCommandHandler
{
    private final IController controller;

    protected AbstractViewCommandHandler()
    {
        super();

        this.controller = SimplePrototypeRegistry.getInstance().getController();
    }

    public final IController getController()
    {
        return this.controller;
    }

    public void execute(final Component.Event event)
    {
        assert null != event : "Parameter 'event' of method 'execute' must not be null";

        executeInternal(event);
    }

    protected abstract void executeInternal(final Component.Event event);
}
