package org.prototype.primecount.view.base;

public final class ComponentNull
{
    public static final ComponentNull SINGLETON = new ComponentNull();

    private ComponentNull()
    {
        super();
    }

    @Override
    public String toString()
    {
        return "null";
    }
}
