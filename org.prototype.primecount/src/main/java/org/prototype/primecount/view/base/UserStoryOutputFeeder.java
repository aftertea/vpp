package org.prototype.primecount.view.base;

import org.prototype.primecount.core.model.IController;
import org.prototype.primecount.view.AbstractPrototypeComponent;
import org.prototype.primecount.view.OutcomeOfUserStory;
import org.prototype.primecount.view.PrimeCountUserStory.IVisitor;

public final class UserStoryOutputFeeder implements IVisitor
{
    private final IController controller;

    public UserStoryOutputFeeder(final IController icontroller)
    {
        super();
        this.controller = icontroller;
    }

    @Override
    public void visitPrototypeComponent(final AbstractPrototypeComponent protoComponent)
    {
        assert null != protoComponent : "Parameter 'protoComponent' of method 'visitProtoComponent' must not be null";

        /**No implementation required so far**/
    }

    @Override
    public void visitOutcomeUserStory(final OutcomeOfUserStory userStoryResult)
    {
        assert null != userStoryResult : "Parameter 'userStoryResult' of method 'visitUserStoryResult' must not be null";

        userStoryResult.setoutput(this.controller.getPrimeCountOutcomeValue()); //NOPMD
    }
}
