package org.prototype.primecount.view;

import org.prototype.primecount.core.controller.PrimeCounterAdapter.IStepTracker;
import org.prototype.primecount.view.common.ComponentOperations;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.AbstractLayout;

@SuppressWarnings("serial")
public class AbstractUserStoryProgressive extends AbstractUserStory
{
    private final IStepTracker tracker;

    protected AbstractUserStoryProgressive(final AbstractComponent vaadinParent, final AbstractUserStory parent, final String caption)
    {
        super(vaadinParent, parent, caption);

        this.tracker = new UIWorkProgressTracker(this);
    }

    @Override
    protected void defineWithin(final AbstractLayout container)
    {
        assert null != container : "Parameter 'container' of method 'defineWithin' must not be null";

        ComponentOperations.addComponents(container, (UIWorkProgressTracker) this.tracker); //NOPMD
    }

    @Override
    public void accept(final IPrototypeComponentVisitor visitor)
    {
        if (visitor instanceof IVisitor)
        {
            ((IVisitor) visitor).visitUserStoryProgressive(); //NOPMD
        }
        else
        {
            super.accept(visitor);
        }
    }

    public interface IVisitor extends IPrototypeComponentVisitor
    {
        void visitUserStoryProgressive();
    }

    @Override
    public void reset()
    {
        /**nothing to do so far*/
    }
}
