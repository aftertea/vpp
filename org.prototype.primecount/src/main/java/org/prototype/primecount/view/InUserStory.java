package org.prototype.primecount.view;

import org.prototype.primecount.view.common.ComponentOperations;
import org.prototype.primecount.view.handler.DefaultValueChangeListener;
import org.prototype.primecount.view.handler.IntegerValueValidator;

import com.vaadin.data.Validator;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
public final class InUserStory extends AbstractPrototypeComponent
{
    private final AbstractTextField txtfUBound;
    private final Label labelUpperBound;

    protected InUserStory(final AbstractPrototypeComponent parent, final IPrototypeComponentVisitor cleaner)
    {
        super(parent, new HorizontalLayout());

        this.txtfUBound = new TextField();
        this.labelUpperBound = new Label("[n]: for all Primes <= n");
        defineWithin(super.layout);
        this.txtfUBound.addValueChangeListener(new DefaultValueChangeListener(cleaner));
        final Validator validator = new IntegerValueValidator();
        this.txtfUBound.addValidator(validator);
        this.txtfUBound.setRequired(true);
    }

    @Override
    protected void defineWithin(final AbstractLayout container)
    {
        if (container instanceof HorizontalLayout)
        {
            ((HorizontalLayout) container).setSpacing(true); //NOPMD
            ((HorizontalLayout) container).setMargin(new MarginInfo(true, false, true, false));
        }
        ComponentOperations.addComponents(container, this.labelUpperBound, this.txtfUBound); //NOPMD
    }

    public String getTextFieldValue()
    {
        return this.txtfUBound.getValue();
    }

    @Override
    public void reset()
    {
        /**nothing to do so far*/
    }
}
