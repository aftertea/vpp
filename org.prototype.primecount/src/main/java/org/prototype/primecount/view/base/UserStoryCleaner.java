package org.prototype.primecount.view.base;

import org.prototype.primecount.view.AbstractPrototypeComponent;
import org.prototype.primecount.view.OutcomeOfUserStory;
import org.prototype.primecount.view.OutcomeOfUserStory.IVisitor;

public final class UserStoryCleaner implements IVisitor, org.prototype.primecount.view.PrimeCountUserStory.IVisitor,
        org.prototype.primecount.view.AbstractUserStoryProgressive.IVisitor
{
    private final AbstractPrototypeComponent protoComponent;

    public UserStoryCleaner(final AbstractPrototypeComponent protoComponentP)
    {
        assert null != protoComponentP : "Parameter 'protoComponent' of 'OutUserStoryCleaner''s ctor must not be null";

        this.protoComponent = protoComponentP;
    }

    @Override
    public void visitPrototypeComponent(final AbstractPrototypeComponent component)
    {
        /**implementation not required so far*/
    }

    @Override
    public void visitOutcomeUserStory()
    {
        if (this.protoComponent instanceof OutcomeOfUserStory)
        {
            ((OutcomeOfUserStory) this.protoComponent).reset(); //NOPMD
        }
    }

    @Override
    public void visitOutcomeUserStory(final OutcomeOfUserStory userStoryResult)
    {
        /** nothing to do so far*/
    }

    @Override
    public void visitUserStoryProgressive()
    {
        this.protoComponent.reset();
    }
}
