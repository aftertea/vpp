package org.prototype.primecount.view;

import org.prototype.primecount.view.common.ComponentOperations;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
abstract class AbstractUserStory extends AbstractPrototypeComponent
{
    private final AbstractComponent vaadinParent;

    protected AbstractUserStory(final AbstractComponent vaadinComponent, final AbstractUserStory parent, final String caption)
    {
        super(parent, new GridLayout(1, 4));

        assert null != caption && !caption.isEmpty() : "Parameter 'caption' of method 'AbstractUserStory' must not be empty";
        assert null != vaadinComponent : "Parameter 'vaadinComponent' of method 'AbstractUserStory' must not be null";

        this.vaadinParent = vaadinComponent;
        setCaption(caption);
        final Label greetingUser = new Label();
        greetingUser.setValue("Hello " + System.getProperty("user.name") + "!");
        ComponentOperations.addComponents(super.layout, greetingUser);
    }

    protected final AbstractComponent getVaadinParent()
    {
        return this.vaadinParent;
    }
}
