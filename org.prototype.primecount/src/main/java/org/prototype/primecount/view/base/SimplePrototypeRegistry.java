package org.prototype.primecount.view.base;

import org.prototype.primecount.core.command.ProtoPrimeControllerCreator;
import org.prototype.primecount.core.model.IController;

public final class SimplePrototypeRegistry
{
    private static SimplePrototypeRegistry instance;

    private IController controller;

    private SimplePrototypeRegistry()
    {
        super();
    }

    public static SimplePrototypeRegistry getInstance()
    {
        synchronized (SimplePrototypeRegistry.class)
        {
            if (null == instance)
            {
                instance = new SimplePrototypeRegistry();
            }
            return instance;
        }
    }

    public IController getController()
    {
        if (null == this.controller)
        {
            this.controller = ProtoPrimeControllerCreator.create();
        }
        return this.controller;
    }
}
