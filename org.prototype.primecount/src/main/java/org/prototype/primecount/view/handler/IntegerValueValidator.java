package org.prototype.primecount.view.handler;

import com.vaadin.data.util.converter.StringToIntegerConverter;

@SuppressWarnings("serial")
public final class IntegerValueValidator extends AbstractValidator<Integer>
{
    private static final String VALDT_MSG = "Integer value expected";

    public IntegerValueValidator()
    {
        super(VALDT_MSG, new StringToIntegerConverter());
    }
}
