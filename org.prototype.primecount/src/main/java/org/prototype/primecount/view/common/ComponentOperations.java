package org.prototype.primecount.view.common;

import org.prototype.primecount.view.AbstractPrototypeComponent;
import org.slf4j.LoggerFactory;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.GridLayout;

public final class ComponentOperations
{
    private ComponentOperations()
    {
        super();
    }

    public static void addComponents(final AbstractLayout layout, final AbstractComponent... components)
    {
        if (null == layout || null == components)
        {
            LoggerFactory.getLogger(ComponentOperations.class).warn("'layout' and 'components' both should not be null"); //NOPMD
            return;
        }
        layout.addComponents(components);
    }

    public static void establishLayout(final AbstractPrototypeComponent component, final AbstractLayout layout)
    {
        assert null != layout : "Parameter 'layout' of method 'establishLayout' must not be null";
        assert null != layout : "Parameter 'layout' of method 'establishLayout' must not be null";

        layout.setSizeFull();
        if (layout instanceof GridLayout)
        {
            ((GridLayout) layout).setSpacing(true); //NOPMD
            ((GridLayout) layout).setMargin(true); //NOPMD
        }
        component.setSizeFull();
        if (layout instanceof AbstractOrderedLayout)
        {
            ((AbstractOrderedLayout) layout).setMargin(new MarginInfo(true, false, false, false));
            ((AbstractOrderedLayout) layout).setSpacing(true); //NOPMD
            component.setSizeUndefined();
        }
    }
}
