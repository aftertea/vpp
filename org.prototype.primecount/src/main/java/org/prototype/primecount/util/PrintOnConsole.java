package org.prototype.primecount.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.slf4j.LoggerFactory;

public final class PrintOnConsole
{
    private static final int ZERO = 0;
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private static int counter = 1;

    private PrintOnConsole()
    {
        super();
    }

    public static void print(final Set<BigInteger> set)
    {
        final List<BigInteger> sortedLst = sortSet(set);
        final StringBuilder strBldr = new StringBuilder(34);
        strBldr.append("\n-> The Prime Set (lsit) is\n");
        sortedLst.forEach(t -> { //NOPMD
                    strBldr.append(t).append(" \t ");
                    if (ZERO == counter % 10)
                    {
                        strBldr.append(LINE_SEPARATOR);
                    }
                    counter++;
                });
        strBldr.trimToSize();
        LoggerFactory.getLogger(PrintOnConsole.class).info(strBldr.toString()); //NOPMD
    }

    private static List<BigInteger> sortSet(final Set<BigInteger> set)
    {
        assert null != set : "Parameter 'set' of method 'sortSet' must not be null";

        final List<BigInteger> list = new ArrayList<>(set);
        Collections.sort(list);
        return Collections.unmodifiableList(list);
    }
}
