package org.prototype.primecount;

import javax.servlet.annotation.WebServlet;

import org.prototype.primecount.view.PrimeCountUserStory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Push(PushMode.AUTOMATIC)
@Theme("mytheme")
@Widgetset("org.prototype.primecount.MyAppWidgetset")
public class Application extends UI
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private static final long serialVersionUID = 1L;

    public Application() //NOPMD
    {
        super();
    }

    @Override
    protected void init(final VaadinRequest vaadinRequest)
    {
        assert null != vaadinRequest : "Parameter 'vaadinRequest' of method 'init' must not be null";

        LOGGER.info("Let's count some primes!");

        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);
        final PrimeCountUserStory userStory = new PrimeCountUserStory(this);
        layout.addComponent(userStory);
        
        /**
         * Add-on developers should note that this method is only meant for
         * the application developer.
         * An add-on should not set the poll interval directly, rather
         * instruct the user to set it.
         */
        setPollInterval(11000);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = Application.class, productionMode = false)
    public static class ApplicationDefaultServlet extends VaadinServlet
    {
        private static final long serialVersionUID = 1L;

        public ApplicationDefaultServlet() //NOPMD
        {
            super();
        }
    }
}
