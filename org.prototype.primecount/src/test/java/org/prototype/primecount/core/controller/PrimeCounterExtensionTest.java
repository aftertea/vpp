package org.prototype.primecount.core.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.prototype.primecount.core.command.ProtoPrimeControllerCreator;
import org.prototype.primecount.core.model.IController;
import org.prototype.primecount.core.model.JobDoneEvent;
import org.prototype.primecount.core.model.JobHasProgressedEvent;
import org.prototype.primecount.core.model.JobJustStartedEvent;
import org.prototype.primecount.foundation.AbstractEventHandler;
import org.prototype.primecount.foundation.SimpleEventManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  TODO incomplete Test case - consider to compare the actual list of primes with the expected one.
 */
public class PrimeCounterExtensionTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PrimeCounterExtensionTest.class);

    private IController controller;
    private PrimeCounterExtension primeCExt;
    private static final BigInteger EXPE_PRIME_COUNT = BigInteger.valueOf(1000);

    @Before
    public void setUp()
    {
        this.controller = ProtoPrimeControllerCreator.create();
        this.primeCExt = new PrimeCounterExtension(1, 7919, this.controller);

        SimpleEventManager.getInstance().attach(new AbstractEventHandler<JobJustStartedEvent>(JobJustStartedEvent.class)
        {
            @Override
            public void handle(final JobJustStartedEvent event)
            {
                /**nothing to do*/
            }
        });

        SimpleEventManager.getInstance().attach(new AbstractEventHandler<JobHasProgressedEvent>(JobHasProgressedEvent.class)
        {
            @Override
            public void handle(final JobHasProgressedEvent event)
            {
                /**nothing to do*/
            }
        });

        SimpleEventManager.getInstance().attach(new AbstractEventHandler<JobDoneEvent>(JobDoneEvent.class)
        {
            @Override
            public void handle(final JobDoneEvent event)
            {
                /**nothing to do*/
            }
        });

        /*TODO this.expectedPrimeList = consumeExpectedValue(getClass().getResourceAsStream(
                "/org/prototype/primecount/core/controller/expected_The1st1000Primes.txt"));
                */
    }

    //TODO implementation not complete
    @SuppressWarnings({"unused" })
    private static List<BigInteger> consumeExpectedValue(final InputStream resourceAsStream)
    {
        assert null != resourceAsStream : "Parameter 'resourceAsStream' of method 'consumeExpectedValue' must not be null";

        final byte[] bytes = new byte[2048];
        try
        {
            resourceAsStream.read(bytes);
        }
        catch (IOException e)
        {
            LOGGER.error("error while consuming the resource expected value", e);
        }
        return Collections.emptyList();
    }

    @Test
    public void test() //NOPMD
    {
        this.primeCExt.run();
        final String outcome = this.controller.getPrimeCountOutcomeValue();
        assertNotNull("expected local variable 'outcome' not to be null", outcome);
        final BigInteger actualPrimeCount = this.controller.getPrimeCountOutcome().getPrimeCount();
        assertEquals("Expected prime count 1000 does not equal actual prime count '" + actualPrimeCount + "'", EXPE_PRIME_COUNT, actualPrimeCount); //NOPMD
    }
}
