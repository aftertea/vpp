org.prototype.primecount
==============

A Prototype project utilising multi-core machine platform, Vaadin and Java.util.concurrent library with and application on building and runnning with Maven and Gradle.
It is also an application on using asynchronous mechanism to publish VM Heap state every some time. 
Moreover, providing a progress bar that advances during the feature run to indicate the progress percentage. It is somehow a fake progressor but it 
should start and stop with the the feature operation and it should advances while the feature is running.

Requirements
--
* [] Servlet 3.0 container.

Workflow (Maven)
========

  * [] `$ mvn install`
    To compile the entire project, run "mvn install".
  * [] `$ mvn jetty:run`
    To run the application, and then 
    open http://localhost:8080/ .
    Use also `http://localhost:8080/?Debug` to debug.

  * [] `$ mvn vaadin:run-codeserver`
    For Debugging client side code
    - run "mvn vaadin:run-codeserver" on a separate console while the application is running
    - activate Super Dev Mode in the debug window of the application

  * [] `$ mvn clean vaadin:compile-theme package`
    To produce a deployable production mode WAR:
    - change productionMode to true in the servlet class configuration (nested in the UI class)
    - run "mvn clean vaadin:compile-theme package"
    - See below for more information. Running "mvn clean" removes the pre-compiled theme.
  * [] `$ mvn jetty:run-war`
    - For testing

    
Using a precompiled theme
-------------------------

When developing the application, Vaadin can compile the theme on the fly when needed,
or the theme can be precompiled to speed up page loads.

  * [] `$ mvn vaadin:compile-theme` 
    To precompile the theme run "mvn vaadin:compile-theme". Note, though, that once
    the theme has been precompiled, any theme changes will not be visible until the
    next theme compilation or running the "mvn clean" target.

** When developing the theme, running the application in the "run" mode (rather than
   in "debug") in the IDE can speed up consecutive on-the-fly theme compilations
   significantly.

** Resources: https://vaadin.com/maven#start



Workflow (Gradle)
========

A - Running the application using Gradle Vaadin Plugin
--

I am still trying to figure out how to solve the missing widgetset problem
  * [1] `$ gradle clean vaadinRun`
  * [0] `$ gradle clean vaadinCompileWidgetset`
    To clean and compile widgetset; Then, pursue [1]. 

B - Running the application using Gradle Gretty Plugin

  * [1] `$ gradle appRun`
    Open http://localhost:8080/org.prototype.primecount/
  * [0] `$ gradle clean vaadinCompileWidgetset`
    To clean and compile widgetset; Then, pursue [1]. 

Bugs
==
* (1) Big N input values will halt the application
* (2) Heap status is not published all the times it gets updated when running the application with `maven jetty:run` 